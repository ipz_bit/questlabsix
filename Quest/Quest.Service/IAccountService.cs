﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Quest.Dto;

namespace Quest.Service
{
    public interface IAccountService : IDomainEntityService<AccountDto>
    {
        AccountDto Identify(string login, [NonEmptyStringValidator] string password);  

        void ChangeLogin(Guid accountId, [NonEmptyStringValidator] string newLogin);

        void ChangePassword(Guid accountId, [NonEmptyStringValidator] string oldPassword, [NonEmptyStringValidator] string newPassword);

		Guid CreateAdministrator(string login, string password);

		Guid CreateScriptwriter(string login, string password);

		Guid CreateUser(string login, string password, string name, string surname, string phone, string email);
    }
}
