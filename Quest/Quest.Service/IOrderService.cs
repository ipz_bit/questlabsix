﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Quest.Dto;

namespace Quest.Service
{
	public interface IOrderService : IDomainEntityService<OrderDto>
	{
		Guid CreateOrder(Guid accountId, Guid scriptId, Guid teamId, Guid cardId, Guid scheduleId);

		void ConfirmOrder(Guid orderId);

		void CancelOrder(Guid orderId);
	}
}
