﻿using System;
using Quest.Dto;

namespace Quest.Service
{
	public interface IScriptService : IDomainEntityService<ScriptDto>
	{
		Guid CreateScript(string title, string description, decimal price);

		void AddPoint(Guid scriptId, Guid pointId);

		string GetHint(Guid scriptId, double x, double y);

		int GetPointsCount(Guid scriptId);
	}
}
