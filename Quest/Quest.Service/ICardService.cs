﻿using System;
using Quest.Dto;

namespace Quest.Service
{
	public interface ICardService : IDomainEntityService<CardDto>
	{
		Guid CreateCard(Guid ownerId, [CardNumberValidator] string cardNumber, [NonEmptyStringValidator] string bankName);
	}
}
