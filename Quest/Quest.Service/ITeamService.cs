﻿using System;
using Quest.Dto;

namespace Quest.Service
{
	public interface ITeamService : IDomainEntityService<TeamDto>
	{
		Guid CreateTeam(Guid accountId, string name);

		void AddTeamMember(Guid teamId, Guid accountId);

		void EditTeamName(Guid teamId, string name);
	}
}
