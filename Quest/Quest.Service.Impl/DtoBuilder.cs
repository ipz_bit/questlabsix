﻿using Quest.Dto;
using Quest.Model;

namespace Quest.Service.Impl
{
	static class DtoBuilder
	{
		public static AccountDto ToDto(this Account account)
		{
			return new AccountDto(account.DomainId, account.Login, account.Password);
		}

		public static CardDto ToDto(this Card card)
		{
			return new CardDto(card.DomainId, card.OwnerName, card.CardNumber, card.BankName);
		}

		public static OrderDto ToDto(this Order order)
		{
			return new OrderDto(
				order.DomainId,
				order.Cost,
				order.Team.DomainId,
				order.OrderStatus.ToString(),
				order.Card.DomainId,
				order.ScriptTitle);
		}

		public static PointDto ToDto(this Point point)
		{
			return new PointDto(point.DomainId, point.X, point.Y, point.Address, point.Hint);
		}

		public static ScheduleDto ToDto(this Schedule schedule)
		{
			ScheduleDto scheduleDto = new ScheduleDto(schedule.DomainId, schedule.Script.DomainId, schedule.TimeBegin, schedule.Duration);
			foreach (var t in schedule.Teams)
			{
				scheduleDto.Teams.Add(t.ToDto());
			}
			return new ScheduleDto(schedule.DomainId, schedule.Script.DomainId, schedule.TimeBegin, schedule.Duration);
		}

		public static ScriptDto ToDto(this Script script)
		{
			return new ScriptDto(script.DomainId, script.Title, script.Description, script.Price);
		}
		
		public static TeamDto ToDto(this Team team)
		{
			var teamDto = new TeamDto(team.DomainId, team.Name, team.members[0].ToDto());
			if (team.members.Count > 1)
			{
				foreach (var member in team.members)
				{
					teamDto.members.Add(member.ToDto());
				}
			}
			return teamDto;
		}
	}
}
