﻿using System;
using System.Collections.Generic;
using System.Linq;

using Quest.Dto;
using Quest.Repository;
using Quest.Model;
using Quest.Exceptions;

namespace Quest.Service.Impl
{
	class TeamService : ITeamService
	{
		private ITeamRepository teamRepository;
		private IAccountRepository accountRepository;

		public TeamService(ITeamRepository teamRepository, IAccountRepository accountRepository)
		{
			this.teamRepository = teamRepository;
			this.accountRepository = accountRepository;
		}

		public IList<Guid> ViewAll()
		{
			return teamRepository.SelectAllDomainIds().ToList();
		}

		public TeamDto View(Guid teamId)
		{
			Team team = ResolveTeam(teamId);
			return team.ToDto();
		}

		public Guid CreateTeam(Guid accountId, string name)
		{
			Team team = teamRepository.FindByName(name);
			if (team != null)
			{
				throw new DuplicateNamedEntityException(typeof(Team), name);
			}
			teamRepository.StartTransaction();

			UserAccount captainAccount = (UserAccount)ResolveAccount(accountId);
			Team newTeam = new Team(Guid.NewGuid(), name, captainAccount);
			teamRepository.Add(newTeam);
			teamRepository.Commit();
			return newTeam.DomainId;
		}

		public void AddTeamMember(Guid teamId, Guid accountId)
		{
			teamRepository.StartTransaction();
			Team team = ResolveTeam(teamId);
			UserAccount account = (UserAccount)ResolveAccount(accountId);
			team.addTeamMember(account);
			teamRepository.Commit();
		}

		public void EditTeamName(Guid teamId, string name)
		{
			teamRepository.StartTransaction();

			Team team = teamRepository.FindByName(name);
			if (team != null)
			{
				throw new DuplicateNamedEntityException(typeof(Team), name);
			}
			team.Name = name;
			teamRepository.Commit();
		}

		private Team ResolveTeam(Guid teamId)
		{
			return ServiceUtils.ResolveEntity(teamRepository, teamId);
		}

		private Account ResolveAccount(Guid accountId)
		{
			return ServiceUtils.ResolveEntity(accountRepository, accountId);
		}
	}
}
