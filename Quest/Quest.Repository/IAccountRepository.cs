﻿using Quest.Model;

namespace Quest.Repository
{
	public interface IAccountRepository : IRepository <Account>
	{
		Account FindByLogin(string login);
	}
}
