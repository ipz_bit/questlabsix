﻿using Quest.Model;

namespace Quest.Repository
{
	public interface IScheduleRepository : IRepository<Schedule>
	{
	}
}
