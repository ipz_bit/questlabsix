﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Quest.Model
{
	public class Script : Utils.Entity
	{
		public List<Point> Checkpoints { private set; get; }
		
		public string Title { set; get; }
		public string Description { set; get; }
		public decimal Price { set; get; }
		public TimeSpan TimeConstraint { set; get; }

		public Script(Guid domainId, string title, string description, decimal price)
			: base(domainId)
		{
			Title = title;
			Description = description;
			Price = price;
			TimeConstraint = new TimeSpan(12, 0, 0);
			Checkpoints = new List<Point>();
		}

		public void addPointWithHint(Point point)
		{
			Checkpoints.Add(point);
		}

		public int getCheckpointsCount()
		{
			return Checkpoints.Count;
		}

		public string getHint(double x, double y)
		{
			Point point = Checkpoints.Find(p => Math.Abs(p.X - x) < 1.5 && Math.Abs(p.Y - y) < 1.5);
			return (object)point == null ? null : point.Hint;
		}
		
		public override string ToString()
		{
			return String.Format("'{0}'\n description: {1}\n price: {2}", Title, Description, Price);
		}
	}
}
