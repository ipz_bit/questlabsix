﻿using System.Data.Entity.ModelConfiguration;

namespace Quest.Repository.EntityFramework.Configurations
{
	abstract class BasicEntityConfiguration<T> : EntityTypeConfiguration<T>
		where T : Quest.Utils.Entity
	{
		protected BasicEntityConfiguration()
		{
			HasKey(e => e.DatabaseId);
			Property(e => e.DomainId).IsRequired();
		}
	}
}
