﻿using Quest.Repository.EntityFramework.RepositoryImplementations;

namespace Quest.Repository.EntityFramework
{
	public static class RepositoryFactory
	{
		public static IAccountRepository MakeAccountRepository(QuestDbContext dbContext)
		{
			return new AccountRepository(dbContext);
		}

		public static ICardRepository MakeCardRepository(QuestDbContext dbContext)
		{
			return new CardRepository(dbContext);
		}

		public static IOrderRepository MakeOrderRepository(QuestDbContext dbContext)
		{
			return new OrderRepository(dbContext);
		}

		public static IPointRepository MakePointRepository(QuestDbContext dbContext)
		{
			return new PointRepository(dbContext);
		}

		public static IScriptRepository MakeScriptRepository(QuestDbContext dbContext)
		{
			return new ScriptRepository(dbContext);
		}

		public static ITeamRepository MakeTeamRepository(QuestDbContext dbContext)
		{
			return new TeamRepository(dbContext);
		}

		public static IScheduleRepository MakeScheduleRepository(QuestDbContext dbContext)
		{
			return new ScheduleRepository(dbContext);
		}
	}
}
