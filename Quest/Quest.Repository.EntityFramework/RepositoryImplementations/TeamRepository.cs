﻿using Quest.Model;
using System.Linq;

namespace Quest.Repository.EntityFramework.RepositoryImplementations
{
	public class TeamRepository : BasicRepository<Team>, ITeamRepository
	{
		public TeamRepository(QuestDbContext dbContext)
			: base(dbContext, dbContext.Teams)
		{
		}

		public Team FindByName(string name)
		{
			return GetDBSet().Where(t => t.Name == name).SingleOrDefault();
		}
	}
}
