﻿using System;
using System.Collections.Generic;

namespace Quest.Dto
{
    public class CardDto :DomainEntityDto<CardDto>
    {
        public string OwnerName { get; private set; }

        public string CardNumber { get; private set; }

        public string BankName { get; private set; }

        public CardDto (Guid domainId, string ownername, string cardnumber, string bankname)
            :base (domainId)
        {
            this.BankName = bankname;
            this.CardNumber = cardnumber;
            this.OwnerName = ownername;
        }

        protected override IEnumerable<object> GetAttributesToIncludeInEqulityCheck()
        {
            return new List<object> { DomainId, BankName, CardNumber, OwnerName };
        }

        public override string ToString()
        {
            return string.Format("DomainId = {0}\nCardId = {1}\nBankName = {2}\nCardNumber = {3}\nOwnerName = {4}", DomainId, BankName,CardNumber, OwnerName );
        }
    }
}
