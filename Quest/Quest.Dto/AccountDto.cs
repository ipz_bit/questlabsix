﻿using System;
using System.Collections.Generic;

namespace Quest.Dto
{
    public class AccountDto : DomainEntityDto<AccountDto>
    {
        public string Login { get; private set; }

        public string Password { get; private set; }

        public AccountDto(Guid domainId, string login, string password)
            : base(domainId)
        {
            this.Login = login;
            this.Password = password;
        }

        protected override IEnumerable<object> GetAttributesToIncludeInEqulityCheck()
        {
            return new List<object> { DomainId, Login, Password };
        }

        public override string ToString()
        {
            return string.Format("AccountId = {0}\nLogin = {1}\nPassword = {2}\n", DomainId, Login, Password);
        }
    }
}
