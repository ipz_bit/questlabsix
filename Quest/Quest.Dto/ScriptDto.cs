﻿using System;
using System.Collections.Generic;

namespace Quest.Dto
{
    public class ScriptDto : DomainEntityDto<ScriptDto>
    {
        private IList<PointDto> Checkpoints;

        public string Title { private set; get; }
        public string Description { private set; get; }
        public decimal Price { private set; get; }
        public TimeSpan TimeConstraint { private set; get; }

        public ScriptDto(Guid domainId, string title, string description, decimal price)
			: base(domainId)
		{
            Title = title;
            Description = description;
            Price = price;
            TimeConstraint = new TimeSpan(12, 0, 0);
            Checkpoints = new List<PointDto>();
        }
        protected override IEnumerable<object> GetAttributesToIncludeInEqulityCheck()
        {
            return new List<object> { DomainId, Title, Description, Price};
        }

        public override string ToString()
        {
            return String.Format("'{0}'\n description: {1}\n price: {2}", Title, Description, Price);
        }

    }
}
