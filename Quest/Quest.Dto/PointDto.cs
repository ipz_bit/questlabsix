﻿using System;
using System.Collections.Generic;

namespace Quest.Dto
{
    public class PointDto : DomainEntityDto<PointDto>
    {
        public double X { private set; get; }
        public double Y { private set; get; }
        public string Address { private set; get; }
        public string Hint { private set; get; }

        public PointDto(Guid domainId, double x, double y, string address, string hint)
			: base(domainId)
		{
            X = x;
            Y = y;
            Address = address;
            Hint = hint;
        }

        protected override IEnumerable<object> GetAttributesToIncludeInEqulityCheck()
        {
            return new List<object> { DomainId, X, Y, Address, Hint };
        }

        public override string ToString()
        {
            return String.Format("DomainId = {0}\n {1} {2} address: {3} hint: {4}",DomainId, X, Y, Address, Hint);
        }
    }
}
