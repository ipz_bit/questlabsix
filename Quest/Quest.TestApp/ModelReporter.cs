﻿using System.IO;
using System.Collections.Generic;
using Quest.Service;
using Microsoft.Practices.Unity;

namespace Quest.TestApp
{
	class ModelReporter
	{
		public ModelReporter(IUnityContainer unityContainer, TextWriter output)
		{
			this.unityContainer = unityContainer;
			this.output = output;
		}

		public void GenerateReport()
		{
			ReportCollection("Accounts", unityContainer.Resolve<IAccountService>());
			ReportCollection("Orders", unityContainer.Resolve <IOrderService>());
			ReportCollection("Scripts", unityContainer.Resolve <IScriptService>());
			ReportCollection("Teams", unityContainer.Resolve <ITeamService>());
			ReportCollection("Schedules", unityContainer.Resolve <IScheduleService>());
			ReportCollection("Cards", unityContainer.Resolve <ICardService>());
			ReportCollection("Points", unityContainer.Resolve <IPointService>());
		}

		private void ReportCollection<TDto>(string title, IDomainEntityService<TDto> service)
			where TDto : Dto.DomainEntityDto<TDto>
		{
			output.WriteLine("==== {0} ==== ", title);
			output.WriteLine();

			foreach (var entityId in service.ViewAll())
			{
				output.Write(service.View(entityId));

				output.WriteLine();
				output.WriteLine();
			}

			output.WriteLine();
		}

		private IUnityContainer unityContainer;
		private TextWriter output;
	}
}
