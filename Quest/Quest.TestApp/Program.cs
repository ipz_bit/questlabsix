﻿using Quest.Dependencies;
using Quest.Repository.EntityFramework;
using System;

using Microsoft.Practices.Unity;

namespace Quest.TestApp
{
	class Program
	{
		static void Main(string[] args)
		{
			try
			{
				using (var dbContext = new QuestDbContext())
				using (var unityContainer = new UnityContainer())
				{
					dbContext.Database.Initialize(true);
					ContainerBoostraper.RegisterTypes(unityContainer, dbContext);

					TestModelGenerator generator = new TestModelGenerator(unityContainer);
					generator.GenerateTestData();
				}

				using (var dbContext = new QuestDbContext())
				using (var unityContainer = new UnityContainer())
				{
					ContainerBoostraper.RegisterTypes(unityContainer, dbContext);

					ModelReporter reportGenerator = new ModelReporter(unityContainer, Console.Out);
					reportGenerator.GenerateReport();
				}
			}
			catch (Exception e)
			{
				Console.WriteLine("{0}: {1}", e.GetType().FullName, e.Message);

				e = e.InnerException;
			}
		}
	}
}
