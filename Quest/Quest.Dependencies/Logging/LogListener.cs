﻿using System;
using System.Diagnostics.Tracing;

using Microsoft.Practices.EnterpriseLibrary.SemanticLogging;
using Microsoft.Practices.Unity;

namespace Quest.Dependencies
{
	public class LogListener : IDisposable
	{
		internal void OnStartup()
		{
			listenerInfo = FlatFileLog.CreateListener("quest_services.log");
			listenerInfo.EnableEvents(Log, EventLevel.LogAlways, QuestEventSource.Keywords.ServiceTracing);

			listenerErrors = FlatFileLog.CreateListener("quest_diagnostic.log");
			listenerErrors.EnableEvents(Log, EventLevel.LogAlways, QuestEventSource.Keywords.Diagnostic);

			Log.StartupSucceeded();
		}

		public void Dispose()
		{
			listenerInfo.DisableEvents(Log);
			listenerErrors.DisableEvents(Log);

			listenerInfo.Dispose();
			listenerErrors.Dispose();
		}


		[Dependency]
		protected QuestEventSource Log { get; set; }

		private EventListener listenerInfo;
		private EventListener listenerErrors;
	}
}
