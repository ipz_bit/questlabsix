﻿using System;

namespace Quest.Exceptions
{
	public class EmptyEntityException : DomainLogicException
	{
		public EmptyEntityException(Type t, Guid id)
			: base(String.Format("Entity with type {0} and id {1} not exist at current context", t.Name, id))
		{ }
	}
}
