﻿using System;

namespace Quest.Exceptions
{
	public class DateInPastException : DomainLogicException
	{
		public DateInPastException(DateTime date)
			: base(String.Format("For create schedule date {0} must be in future", date.ToString()))
		{ }
	}
}
