﻿using System;

namespace Quest.Exceptions
{
	public class TeamAlreadyRegisteredException : DomainLogicException
	{
		public TeamAlreadyRegisteredException(string teamName, Guid scheduleId)
			: base(String.Format("Team \"{0}\" already registered to schedule with id: {1}", teamName, scheduleId))
		{ }
	}
}
