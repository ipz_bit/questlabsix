﻿using System;

namespace Quest.Exceptions
{
	public class RemovingUnexistingTeamException : DomainLogicException
	{
		public RemovingUnexistingTeamException(string teamName, Guid scheduleId)
			: base(String.Format("Team \"{0}\" not registered for schedule with id: {1}", teamName, scheduleId))
		{ }
 	}
}
