﻿using System;

namespace Quest.Exceptions
{
	public class AccountNotExistException : DomainLogicException
	{
		public AccountNotExistException(Guid id)
			: base (String.Format("Account with id: {0} not exist", id.ToString()))
		{ }
	}
}
