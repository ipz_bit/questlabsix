﻿using System;

namespace Quest.Exceptions
{
	public class EmptyHintPointException : DomainLogicException
	{
		public EmptyHintPointException(string address)
			: base(String.Format("Point with address {0} has no hint", address))
		{ }
	}
}
