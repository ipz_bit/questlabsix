﻿using System;

namespace Quest.Exceptions
{
	public class OrderWrongStateException : DomainLogicException
	{
		public OrderWrongStateException(string teamName, string scriptTitle, string state)
			: base(String.Format("Order for script: \"{0}\" for team \"{1}\" already has state {2}", scriptTitle, teamName, state))
		{ }
	}
}
