﻿using System;

namespace Quest.Exceptions
{
	public class DomainLogicException : Exception
	{
		public DomainLogicException(string message)
			: base(message)
		{ }
	}
}
