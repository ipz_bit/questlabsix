﻿using System;

namespace Quest.Exceptions
{
	public class PasswordMissmatchException : DomainLogicException
	{
		public PasswordMissmatchException(string login)
			:base (string.Format("Wrong password for login {0}", login))
		{}
	}
}
