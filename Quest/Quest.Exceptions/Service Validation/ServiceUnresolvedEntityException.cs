﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Quest.Exceptions.Service_Validation
{
    public class ServiceUnresolvedEntityException : ServiceValidationException
    {
        public ServiceUnresolvedEntityException(Type entityType, Guid entityId)
            : base(
                 string.Format(
                     "Unresolved entity #{1} of type {0}",
                     entityType.ToString(), entityId
                        )
                     )
        { }
      }

}
