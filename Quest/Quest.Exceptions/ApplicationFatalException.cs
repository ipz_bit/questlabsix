﻿using System;

namespace Quest.Exceptions
{
    public class ApplicationFatalException: Exception
    {
        public ApplicationFatalException(string message)
            :base(message)
        {
        }
    }
}
