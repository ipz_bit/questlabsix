﻿using System;
using Microsoft.Practices.EnterpriseLibrary.Validation;
using Microsoft.Practices.EnterpriseLibrary.Validation.Validators;

namespace Quest.Utils.Validators
{
	[AttributeUsage(AttributeTargets.Property | AttributeTargets.Field | AttributeTargets.Parameter)]
	public class CardNumberValidator : ValidatorAttribute
	{
		protected override Validator DoCreateValidator(Type targetType)
		{
			const string EmailRegex = @"[0-9]{13,16}";
			return new RegexValidator(EmailRegex);
		}
	}
}
