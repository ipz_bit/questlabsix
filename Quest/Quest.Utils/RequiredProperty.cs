﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IPZ_2._0.Utils
{
	public class RequiredProperty<T> : AbstractProperty
	{
		public T Value
		{
			get { return _value; }
			set
			{
				if (value == null)
					throw new ArgumentNullException(ParamName);

				_value = value;
			}
		}

		public RequiredProperty(string paramName)
			: base(paramName)
		{
		}

		private T _value;
	}
}
